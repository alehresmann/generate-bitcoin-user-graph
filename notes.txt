todo: read more efficiently:

https://stackoverflow.com/questions/26736742/efficiently-reading-a-very-large-text-file-in-c

in get user graph, for every transaction I use a find() op on tx_clusters. this is because leafs are not clustered, hence it is possible some addresses arent in tx_clusters. does this cause inefficiency ? is it worth clustering the leafs, so as to replace find() with at() ? they have same complexity.
