## What is this ?
This program generates the user graph of the bitcoin blockchain in a csv file. For every identified user, you will get a list of users to whom this user interacted with. I may add more to be able to fetch raw transactions from each interaction, but for now, this was enough for my project.
With it, you can draw two things:
* A list of users and all their found addresses.
* A list of users and all the users they have been found to interact with.

The script is based on the algorithm described in **Maesa, D. D. F., Marino, A., & Ricci, L. "Uncovering the Bitcoin Blockchain: An Analysis of the Full Users Graph", doi:10.1109/dsaa.2016.52**, which itself is based on rules extensively described in **R. Fergal and M. Harrigan, “An analysis of anonymity in the bitcoin system”, arXiv:1107.4524**.


## How do I build this ?
The only requirement are the boost library, and a compiler. With g++ for instance, you can just build with ``g++ get_user_graph.cpp``

## What is the input required ?
It requires two csv input files with no header.
The first describes the input of transactions, and it is of the following format:``txID, [ignored_column], [ignored_column], [ignored_column], addrID, [ignored_column]``. As can be guessed, only the first and the fifth column, that is, the txID (transaction ID), and the addrID (address ID), are used. In our examples, we refer to this file as ``txin.dat``.

The second input describes the output of transactions, and is of the following format: ``txID, [ignored_column], addrID, [ignored_column]``. Similarly to txin.dat, we only look at the first and fourth column. We refer to this file as ``txout.dat``.

The reason for this format is because it is exactly the format of the txin.dat, provided by Kondor D from MIT, [available here](https://senseable2015-6.mit.edu/bitcoin/).

I may eventually implement a way of pulling directly from the blockchain but for now, use the dataset above.

## What are the output file formats ?
user-clusters graph: ``userID, [comma-separated-list-of-addrs]``
user-interaction graph: `` userID, [comma-separated-list-of-userID]``

Both files have no header, and the comma-separated lists are contained within brackets.

## What are some things to keep in mind about the results generated?
* Sometimes, multiple users may collectively sign a transaction, despite not being the same user. This algorithm assumes that a multi-input transaction is always done by a single user. This is not always the case, but is a reasonable assumption to make. If you wish to read more on this, see the **"Uncovering the Bitcoin Blockchain: An Analysis of the Full Users Graph"** paper mentioned above.
* Users sometimes may have received bitcoins in some address, but never spent them. This algorithm ignores such output addresses, and does not include them in its clusters. This is because it would mean merely adding one cluster for every such output address, which have zero interaction with any users except its input user. (In other words, they are leaves in the user-graph). If you want to include them in your user-graph, you will have to add this yourself to the ``load_line_out()`` function. It should not be too hard, imitate the passes in the ``load_line_in()`` function.

## Is this intelligently optimised for low memory usage ?
Not at all. Not only do I not have the skills required for such optimisation, but I specifically designed it without worrying about optimisation, as I figured I could just run it on some powerful machine with lots of RAM. The CPU doesn't need to be top of the line. Personally, I merely used a high-end droplet on [DigitalOcean](https://www.digitalocean.com/).

## Can I get some examples before using this ?
Getting the user-clusters in a ``user_clusters.csv`` file, using the ``fabricated_in.csv`` and ``fabricated_out.csv`` file:

``./a.out fabricated_in.csv fabricated_out.csv user_clusters.csv``

Getting only the user-interaction in a ``user_interaction.csv`` file, using the ``fabricated_in.csv`` and ``fabricated_out.csv`` file:

``./a.out fabricated_in.csv fabricated_out.csv . user_interaction.csv``

Getting both:
``./a.out fabricated_in.csv fabricated_out.csv user_clusters.csv user_interaction.csv``

This will get you the output for the following fabricated example (edges are transactions, nodes are users):
![Diagram of a fabricated user-interaction graph](fabricated_graph.jpg?raw=true "Fabricated Graph")

## I have more questions !
This is released under the GNU general Public License 3.0. For any additional questions, contact Anne-Laure Ehresmann at al@ehresmann.eu
