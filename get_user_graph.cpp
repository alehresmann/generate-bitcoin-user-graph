// reads a csv file in the following format: transaction ID, input_seq, previous
// transaction ID, previous output sequence, address ID, sum aka, reads txin.dat
// from https://senseable2015-6.mit.edu/bitcoin/

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <iostream>
#include <map>
#include <ostream>
#include <set>
#include <sstream>
#include <string>
#include <unordered_set>
#include <vector>

struct transaction {
public:
  std::set<double> in_addr = {};
  std::set<double> out_addr = {};
};

struct user {
public:
  std::set<double> addrs = {};
  std::map<double, int> user_connected_to = {};
};

void print(transaction t) {
  std::cout << "{\t[";
  for (auto i : t.in_addr)
    std::cout << i << ",";
  std::cout << "]\n\t[";
  for (auto o : t.out_addr)
    std::cout << o << ",";
  std::cout << "]\n";
}

void print(std::map<double, user> *user_cluster) {
  for (auto &user : *user_cluster) {
    std::stringstream s;
    for (auto &addr : user.second.addrs) {
      s << addr << ", ";
    }
    std::cout << user.first << ": " << s.str() << std::endl;
  }
}

void load_line_in(std::map<double, transaction> *ts,
                  std::map<double, double> *tx_cluster, double *user_count,
                  std::string line) {
  std::vector<std::string> d_t;
  boost::split(d_t, line, boost::is_any_of("\t"));
  if (d_t.size() < 6) {
    std::cout
        << "Something went wrong with the line I just read, unexpected format: "
        << line << std::endl;
    std::exit(0);
  }

  double txid = std::stod(d_t[0]);
  double in_addr = std::stod(d_t[4]);
  // transactions
  if (ts->find(txid) == ts->end())
    ts->insert(std::pair<double, transaction>(txid, transaction()));
  ts->at(txid).in_addr.insert(in_addr);

  // clusters aka users. this serves to find connected components in the graph.
  double cluster = *user_count;
  // if some transaction was already clustered, and that cluster is a smaller
  // value than the current cluster id, everyone else should also go in this
  // cluster.
  for (auto &addr : ts->at(txid).in_addr) {
    auto addr_clust = tx_cluster->find(addr);
    if (!(addr_clust == tx_cluster->end()) and addr_clust->second < cluster) {
      cluster = addr_clust->second;
    }
  }

  // if no addr belonged to a previously established cluster, make a new one and
  // increment the count.
  if (cluster == *user_count)
    (*user_count)++;
  // insert everyone in this cluster.
  for (auto &addr : ts->at(txid).in_addr) {
    // note: if you try and insert a pair in a map for which the key already has
    // a value, this call will be ignored and the previous value will be kept.
    tx_cluster->insert(std::pair<double, double>(addr, cluster));
  }
}

void load_line_out(std::map<double, transaction> *ts, double *user_count,
                   std::string line) {
  // assumes load_line_out() was already executed.
  //  note: if user wasn't clustered, this is a user which received bitcoin but
  // has not spent it. We don't know if this is a previously known user or a new
  // one. we thus don't include it in our clusters.
  std::vector<std::string> d_t;
  boost::split(d_t, line, boost::is_any_of("\t"));
  if (d_t.size() < 4) {
    std::cout
        << "Something went wrong with the line I just read, unexpected format: "
        << line << std::endl;
    std::exit(0);
  }

  double txid = std::stod(d_t[0]);
  double out_addr = std::stod(d_t[2]);
  // transactions
  // if transaction had no input, this was a mining transaction. Ignore. maybe
  // change this in other versions.
  if (ts->find(txid) != ts->end())
    ts->at(txid).out_addr.insert(out_addr);
}

void load_transactions(std::string txin, std::string txout,
                       std::map<double, transaction> *ts,
                       std::map<double, double> *tx_cluster) {

  std::ifstream txin_f(txin);
  std::ifstream txout_f(txout);
  std::string linebuffer;
  // current number of users found. equivalent to num of values in tx_cluster.
  double *user_count = new double(0);

  std::cout << "reading transactions in...\n";
  while (txin_f && getline(txin_f, linebuffer)) {
    if (linebuffer.length() == 0)
      continue;
    load_line_in(ts, tx_cluster, user_count, linebuffer);
  }
  std::cout << "reading transactions out...\n";
  while (txout_f && getline(txout_f, linebuffer)) {
    if (linebuffer.length() == 0)
      continue;
    load_line_out(ts, user_count, linebuffer);
  }
  delete (user_count);
}

void group_clusters(std::map<double, user> *user_cluster,
                    std::map<double, double> *tx_cluster) {
  // note: the following ignores users which are only on the receiving end of
  // transactions, and who have never spent it. This is to avoid having many
  // leafs in the user graph.
  for (auto tx : *tx_cluster) {
    if (user_cluster->find(tx.second) == user_cluster->end())
      user_cluster->insert(std::pair<double, user>(tx.second, user()));
    user_cluster->at(tx.second).addrs.insert(tx.first);
  }
}

void get_user_graph(std::map<double, transaction> *ts,
                    std::map<double, double> *tx_cluster,
                    std::map<double, user> *user_cluster) {
  for (auto tx : *ts) {
    for (auto out_addr : tx.second.out_addr) {
      if (tx_cluster->find(out_addr) != tx_cluster->end()) {
        double user_in_id = tx_cluster->at(*tx.second.in_addr.begin());
        double user_out_id = tx_cluster->at(out_addr);
        // change this to weights eventually.
        (*user_cluster)[user_in_id].user_connected_to[user_out_id]++;
        (*user_cluster)[user_out_id].user_connected_to[user_in_id]++;
      }
    }
  }
}

void write_user_clusters(std::map<double, user> *user_cluster,
                         std::string filename) {
  std::ofstream file;
  file.open(filename, std::ios_base::app);

  for (auto u : *user_cluster) {
    file << u.first << "\t[";
    bool first = true;
    for (auto addr : u.second.addrs) {
      if (!first)
        file << " ";
      else
        first = false;
      file << boost::lexical_cast<std::string>(addr);
    }
    file << "]\n";
  }
  file.close();
}

void write_user_interaction(std::map<double, user> *user_cluster,
                            std::string filename) {
  std::ofstream file;
  file.open(filename, std::ios_base::app);
  for (auto usr : *user_cluster) {
    file << usr.first << "\t[";
    bool first = true;
    for (auto user_w_pair : usr.second.user_connected_to) {
      if (!first)
        file << " ";
      else
        first = false;
      file << boost::lexical_cast<std::string>(user_w_pair.first) << ":"
           << user_w_pair.second;
    }
    file << "]\n";
  }
  file.close();
}

int main(int argc, char *argv[]) {
  if (argc < 3) {
    std::cout << "You need to provide me with two input files!" << std::endl;
    std::exit(0);
  }

  std::map<double, transaction> *ts = new std::map<double, transaction>;
  // tx_id -> cluster_id
  std::map<double, double> *tx_cluster = new std::map<double, double>;
  // cluster_id -> user
  std::map<double, user> *user_cluster = new std::map<double, user>;
  std::cout << "Reading files...\n";
  load_transactions(argv[1], argv[2], ts, tx_cluster);

  std::cout << "Finished first pass. Grouping clusters together...\n";
  group_clusters(user_cluster, tx_cluster);

  if (argc > 3 and strcmp(argv[3], ".") != 0) {
    std::cout << "Writing user clusters to " << argv[3] << std::endl;
    write_user_clusters(user_cluster, argv[3]);
  }

  if (argc > 4 and strcmp(argv[4], ".") != 0) {
    std::cout << "Getting user interactiong graph...\n";
    get_user_graph(ts, tx_cluster, user_cluster);
    std::cout << "Writing user interaction graph to " << argv[4] << std::endl;
    write_user_interaction(user_cluster, argv[4]);
  }

  delete (ts);
  delete (user_cluster);
  delete (tx_cluster);
}
